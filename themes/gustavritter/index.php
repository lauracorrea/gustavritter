<?php get_header(); ?>

<section class="carousel">
	<div class="container">
        <div id="header-carousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                $args = array('post_type'=>'banner', 'showposts'=>5);
                $my_slider = get_posts( $args );
                $count = 0 ; if($my_slider) : foreach($my_slider as $post) : setup_postdata( $post );
                ?>
                <li data-target="#header-carousel" data-slide-to="<?= $count; ?>" <?php if($count == 0): ?> class="active"<?php endif; ?>></li>
                <?php $count++; endforeach; endif; ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php $cont = 0 ; if($my_slider) : foreach($my_slider as $post) : setup_postdata( $post );?>
                <div class="item <?php if($cont == 0) echo "active"; ?>">
                    <div class="slide-image"><?php the_post_thumbnail('tamanho-banner'); ?></div>
                </div>
                <?php $cont++; endforeach; endif; ?>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#header-carousel" role="button" data-slide="prev"></a>
            <a class="right carousel-control" href="#header-carousel" role="button" data-slide="next"></a>
        </div>
        <!-- Sessão Núcleos-->
        <section class="sessao nucleos">
            <h6 class="title">NÚCLEOS</h6>
        
            <div class="row">
                <div class="col-md-4">
                     <img class="img-responsive" src="<?php bloginfo('template_directory');?>/assets/img/musica-banner.jpg ">
                </div>

                <div class="col-md-4">
                    <img class="img-responsive" src="<?php bloginfo('template_directory');?>/assets/img/danca-banner.jpg ">
                </div>

                <div class="col-md-4">
                   <img class="img-responsive" src="<?php bloginfo('template_directory');?>/assets/img/teatro-banner.jpg ">
              </div>
            </div>
        </section>

        <!-- Sessão Notícia-->

        <section class="sessao noticia">
            <h6 class="title">NOTÍCIAS</h6>
            <!-- LOOP -->
            <div class="container">
                <!-- CONFIGURAR TIPO DE POST -->
                <?php $myposts = new WP_Query( array('cat' => '-5', 'posts_per_page' => 3));  ?>
                <!-- INICIAR O LOOP -->
                <?php if($myposts->have_posts()): while( $myposts->have_posts()):?>
                <?php $myposts->the_post();?>
                <!-- ESTILO DO POST -->
                <article class="col-md-4 post">
                    <figure><?php the_post_thumbnail('', array('class' => 'img-responsive')); ?></figure>
                    <strong><?php the_category(' '); ?></strong>
                    <p><a href="<?php the_permalink(); ?>"><?php the_title();?></a></p>
                </article>
                <?php endwhile; ?>
                <?php else: ?> 
                    <p>Não existem posts</p>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </section>

        <!-- Sessão Tv Gustav -->
        <section class="sessao tv">
            <h6 class="title">TV GUSTAV</h6>
           
            <div class="row">
                <div class="col-sm-6">
                    <h4>Interatividade</h4>
                    <p>Conheça o canal da Seduce Goiás no Youtube. Acompanhe as novidades no <a href="https://www.youtube.com/channel/UC8Cs9PdF43zq0cF0tmz7ZNQ"> www.youtube.com/seducego.</a></p>
                    <p>Inscreva-se no canal da Seducego, você assiste às reportagens, campanhas de estudo e vídeos com informações relacionadas à educação e cursos ofertados pela instituição.</p>
                    <a href="#" class="custom-botao">Todos os Videos</a>
            </div>
            <div class="videos">
                <?php $videos = new WP_Query( array('category_name' => 'video', 'posts_per_page' => 1));   ?>
                <?php if($videos->have_posts()): while( $videos->have_posts()): $videos->the_post();?>
                    <article class="container">
                        <div class="video-embed">
                            <?php the_content(); ?>
                        </div>
                        <div class="video-info">
                            <h3 class="section-title"><?php the_title();?></h3>
                            <p><?= get_post_meta($post->ID, 'Descrição', true); ?></p>
                        </div>
                    </article>
                <?php endwhile; endif; ?>
            </div>
    </section>
</section>

<?php get_footer(); ?>