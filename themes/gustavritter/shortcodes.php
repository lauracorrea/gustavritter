<!-- LOOP -->

<section id="todas-notícias">
	<!-- CONFIGURAR TIPO DE POST -->
	<?php $myposts = new WP_Query( array('category_name' => 'Notícias',));	?>
	<!-- INICIAR O LOOP -->
	<?php if($myposts->have_posts()): while( $myposts->have_posts()):?>
	<?php $myposts->the_post();?>
	<!-- ESTILO DO POST -->
	<article class="container">
		<figure><?php the_post_thumbnail(); ?></figure>
		<h3><?php the_title();?></h3>
		<p><?php the_content(); ?></p>
		<b><?php the_author(); ?></b>
		<em><?php the_date(); ?></em>
		<strong><?php the_category(); ?></strong>

	</article>
	<?php endwhile; ?>
	<?php else: ?> 
		<p>Não existem posts</p>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
</section>