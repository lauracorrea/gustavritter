<?php get_header(); ?>

<?php if(have_posts()):	the_post();?>
    <article class="container">
        <header>
            <h2><?php the_title();?></h2>
        </header>
        <main>
            <p><?php the_content(); ?></p>
        </main>
    </article>
<?php endif; ?>

<?php get_footer(); ?>