<?php get_header(); ?>

<?php if(have_posts()):	the_post();?>
    <article class="container">
        <header class="page-title">
            <h2><?php the_title();?></h2>
        </header>
        <main class="page-main">
            <p><?php the_content(); ?></p>
            <p class="date"><?php the_date(); ?></p>
        </main>
    </article>
<?php endif; ?>
<iframe
  width= 100%
  height="500"
  frameborder="0" style="border:0"
  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAosggKIXh0_hUrPwCBZ57xODL8MKAr4T0
    &q=Instituto+Gustav+Ritter,Goiania+Go" allowfullscreen>
</iframe>

<?php get_footer(); ?>