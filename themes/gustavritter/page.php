<?php get_header(); ?>

<?php if(have_posts()):	the_post();?>
    <article class="container">
        <header class="page-title">
            <h2><?php the_title();?></h2>
        </header>
        <main class="page-main">
            <p><?php the_content(); ?></p>
            <p class="date"><?php the_date(); ?></p>
        </main>
    </article>
<?php endif; ?>

<?php get_footer(); ?>