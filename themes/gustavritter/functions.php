<?php 
/* SUPORTE PARA IMAGENS DESTACADAS */
function add_suport_theme(){
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'tamanho-banner', 1287, 368, true );
}
add_action('after_setup_theme','add_suport_theme');

/* REGISTRAR MENU */
register_nav_menus( array(
    'primary' => 'primary',
));

/* ADICIONAR SCRIPTS / CSS */
function wp_seduce_scripts() {
  // Carregando CSS header
  wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
  wp_enqueue_style( 'style', get_stylesheet_uri() );
  // Carregando Scripts header

  //Carregando no footer
  wp_enqueue_script('jquery-3.1.1.min', get_template_directory_uri().'/assets/js/jquery-3.1.1.min.js', array('jquery'), '', true );
  wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/assets/js/bootstrap.min.js', array('jquery') , '', true );
  //wp_enqueue_script('main.js', get_template_directory_uri().'/assets/js/main.js', array('jquery') , '', true);
}

add_action( 'wp_enqueue_scripts', 'wp_seduce_scripts' );

// Register Custom Post Type
add_action( 'init', 'custom_post_type', 0 );
function custom_post_type() {
  $labels = array(
    'name'                  => _x( 'banner', 'Post Type General Name', 'text_domain' ),
    'singular_name'         => _x( 'banner', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'             => __( 'banner', 'text_domain' ),
    'name_admin_bar'        => __( 'banner', 'text_domain' ),
    'archives'              => __( 'Item Archives', 'text_domain' ),
    'attributes'            => __( 'Item Attributes', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
    'all_items'             => __( 'Todos', 'text_domain' ),
    'add_new_item'          => __( 'Adicionar Novo banner', 'text_domain' ),
    'add_new'               => __( 'Adicionar Novo', 'text_domain' ),
    'new_item'              => __( 'Novo banner', 'text_domain' ),
    'edit_item'             => __( 'Editar banner', 'text_domain' ),
    'update_item'           => __( 'Update banner', 'text_domain' ),
    'view_item'             => __( 'Ver banner', 'text_domain' ),
    'view_items'            => __( 'Ver banners', 'text_domain' ),
    'search_items'          => __( 'Procurar banner', 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
    'items_list'            => __( 'Items list', 'text_domain' ),
    'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
  );
  $args = array(
    'label'                 => __( 'banner', 'text_domain' ),
    'description'           => __( 'Post Type Description', 'text_domain' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'thumbnail', 'custom-fields', ),
    'taxonomies'            => array( 'category', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-slides',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,    
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'banner', $args );

}
