<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
    <meta name="keywords" content="Educa360">
    <meta name="author" content="SEDUCE - GOIÁS">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Portal Educa360 com tutoriais dicas e informações sobre os sistemas e procedimentos internos da Secretaria de Educação Cultura e Esporte de Goiás">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico">
	<title><?php wp_title(); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bubbler+One" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div class="container">
            <img class="img-responsive" src="<?php bloginfo('template_directory');?>/assets/img/img-header.png ">
            <nav class="menu-principal">
        	    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) );?>
            </nav>
        </div>
    </header>
</body>